# Treetags

## Requirements

* Linux Operating System
* GCC
* Sqlite3
* Make

## Setup

* Extract folder to ~/Dev/treetags
* Compile via make build, followed by make link, followed by make clean

## Basic Use

* Run the treetags executable created by the build/linking process
* Run treetags help to get a full list of commands
* Tags are separated by commas
* A path is made up of the collection tags, separated by commas, and path levels separated by /
* The treetags print command needs to be given a pass ending with a /, indicating that all contained files should be displayed.
