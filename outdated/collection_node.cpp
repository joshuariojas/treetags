#include "collection_node.h"

Collection_Node::Collection_Node(void)
{
	id = 0;
	parent_node = this;
	tags = "root";
}

Collection_Node::Collection_Node(int id, Collection_Node *parent_node, std::string tags)
{
	this->id = id;
	this->parent_node = parent_node;
	this->tags = tags;
}

int Collection_Node::getId()
{
	return id;
}

Collection_Node Collection_Node::getParent()
{
	return *parent_node;
}

std::string Collection_Node::getTags() {
	return tags;
}

void Collection_Node::updateTags(std::string tags)
{
	this->tags = tags;
}
