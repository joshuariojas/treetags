// collection_node.h
#ifndef COLLECTION_NODE_H
#define COLLECTION_NODE_H

#include <iostream>

class Collection_Node
{
	int id;
	Collection_Node *parent_node;
	std::string tags;

public:
	Collection_Node(void);
	Collection_Node(int id, Collection_Node *parent_node, std::string tags);
	int getId();
	Collection_Node getParent();
	std::string getTags();
	void updateTags(std::string tags);
};

#endif
