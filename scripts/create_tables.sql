CREATE TABLE treetags_physical_links(
	file_id			INTEGER NOT NULL PRIMARY KEY ASC,
	physical_path	TEXT NOT NULL UNIQUE
);
CREATE TABLE treetags_files(
	file_id			INTEGER NOT NULL UNIQUE,
	collection_id 	INTEGER NOT NULL,
	file_tags		TEXT,
	PRIMARY KEY(collection_id, file_tags),
	FOREIGN KEY(file_id) REFERENCES treetags_physical_links(file_id),
	FOREIGN KEY(collection_id) REFERENCES treetags_collection_ids(collection_id)
);
CREATE TABLE treetags_collection_ids(
	collection_id	INTEGER NOT NULL PRIMARY KEY ASC
);
CREATE TABLE treetags_collections(
	collection_id 	INTEGER NOT NULL UNIQUE,
	parent_id 		INTEGER NOT NULL,
	collection_tags	TEXT,
	PRIMARY KEY(parent_id, collection_tags),
	FOREIGN KEY(collection_id) REFERENCES treetags_collection_ids(collection_id),
	FOREIGN KEY(parent_id) REFERENCES treetags_collection_ids(collection_id)
);