#include "sqlite_callbacks.h"

int SQLite_Callbacks::parent_id;
int SQLite_Callbacks::collection_id;
int SQLite_Callbacks::file_id;
int SQLite_Callbacks::file_count;
bool SQLite_Callbacks::unique_collection;
bool SQLite_Callbacks::unique_file;
bool SQLite_Callbacks::print_files;
std::string SQLite_Callbacks::ctags;
std::string SQLite_Callbacks::ftags;
std::string SQLite_Callbacks::file_link;

void SQLite_Callbacks::initialize_collection_fields()
{
	parent_id = -1;
	collection_id = -1;
	unique_collection = true;
	ctags = "";
}

void SQLite_Callbacks::initialize_file_fields()
{
	collection_id = -1;
	file_id = -1;
	file_count = 0;
	unique_file = true;
	print_files = false;
	ftags = "";
	file_link = "";
}

int SQLite_Callbacks::find_collection_callback(void *data, int argc, char **argv, char **azcolName)
{
	if (argc == 3)
	{
		if (unique_collection && !ctags.empty())
		{
			unique_collection = false;
		}
		
		ctags = argv[1];
		std::string value(argv[0]);
		std::stringstream ss(value);
		int temp;
		ss >> temp;

		if (!ss)
		{
			std::cout << "Error: Unexpected results returned by query." << std::endl;
			return 0;
		}

		collection_id = temp;
		std::string value2(argv[2]);
		std::stringstream ss2(value2);
		ss >> temp;

		if (!ss2)
		{
			std::cout << "Error: Unexpected results returned by query." << std::endl;
			return 0;
		}

		parent_id = temp;
	} 

	return 0;
}

int SQLite_Callbacks::find_collection_id_callback(void *data, int argc, char **argv, char **azcolName)
{
	if (argc == 1)
	{
		std::string value(argv[0]);
		std::stringstream ss(value);
		int temp;
		ss >> temp;

		if (!ss)
		{
			std::cout << "Error: Unexpected results returned by query." << std::endl;
			return 0;
		}

		collection_id = temp;
	}

	return 0;
}

int SQLite_Callbacks::find_file_callback(void *data, int argc, char **argv, char **azcolName)
{
	if (argc == 3)
	{
		if (unique_file && !ftags.empty())
		{
			unique_file = false;
		}
		
		ftags = argv[1];
		std::string value(argv[0]);
		std::stringstream ss(value);
		int temp;
		ss >> temp;

		if (!ss)
		{
			std::cout << "Error: Unexpected results returned by query." << std::endl;
			return 0;
		}

		file_id = temp;
		std::string value2(argv[2]);
		std::stringstream ss2(value2);
		ss >> temp;

		if (!ss2)
		{
			std::cout << "Error: Unexpected results returned by query." << std::endl;
			return 0;
		}

		collection_id = temp;
	} 

	return 0;
}

int SQLite_Callbacks::find_file_id_callback(void *data, int argc, char **argv, char **azcolName)
{
	if (argc == 1)
	{
		std::string value(argv[0]);
		std::stringstream ss(value);
		int temp;
		ss >> temp;

		if (!ss)
		{
			std::cout << "Error: Unexpected results returned by query." << std::endl;
			return 0;
		}

		file_id = temp;
	}

	return 0;
}

int SQLite_Callbacks::find_file_link_callback(void *data, int argc, char **argv, char **azcolName)
{
	if (argc == 1)
	{
		file_link = argv[0];
	}

	return 0;
}

int SQLite_Callbacks::last_insert_callback(void *data, int argc, char **argv, char **azcolName)
{
	if (argc == 0)
	{
		std::cout << "Error: Insert statement into collection_ids likely failed." << std::endl;
		collection_id = 0;
	}
	else 
	{
		std::string value(argv[0]);
		std::stringstream ss(value);
		int temp;
		ss >> temp;

		if (!ss)
		{
			std::cout << "Error: Unexpected results returned by query." << std::endl;
			return 0;
		}

		collection_id = temp;
	}

	return 0;
}

int SQLite_Callbacks::last_insert_file_callback(void *data, int argc, char **argv, char **azcolName)
{
	if (argc == 0)
	{
		std::cout << "Error: Insert statement into file_ids likely failed." << std::endl;
		collection_id = 0;
	}
	else 
	{
		std::string value(argv[0]);
		std::stringstream ss(value);
		int temp;
		ss >> temp;

		if (!ss)
		{
			std::cout << "Error: Unexpected results returned by query." << std::endl;
			return 0;
		}

		file_id = temp;
	}

	return 0;
}

int SQLite_Callbacks::not_selection_callback(void *data, int argc, char **argv, char **azcolName)
{
	return 0;	
}
