#ifndef SQLITE_CALLBACKS_H
#define SQLITE_CALLBACKS_H

#include <iostream>
#include <sstream>

struct SQLite_Callbacks
{
	static int parent_id;
	static int collection_id;
	static int file_id;
	static int file_count;
	static bool unique_collection;
	static bool unique_file;
	static bool print_files;
	static std::string ctags;
	static std::string ftags;
	static std::string file_link;

	static void initialize_collection_fields();
	static void initialize_file_fields();
	
	static int find_collection_callback(void *data, int argc, char **argv, char **azcolName);
	static int find_collection_id_callback(void *data, int argc, char **argv, char **azcolName);

	static int find_file_callback(void *data, int argc, char **argv, char **azcolName);
	static int find_file_id_callback(void *data, int argc, char **argv, char **azcolName);
	static int find_file_link_callback(void *data, int argc, char **argv, char **azcolName);

	static int last_insert_callback(void *data, int argc, char **argv, char **azcolName);
	static int last_insert_file_callback(void *data, int argc, char **argv, char **azcolName);
	static int not_selection_callback(void *data, int argc, char **argv, char **azcolName);
};

extern SQLite_Callbacks CB;

#endif
