#include "treetags_shared.h"
#include "treetags_commands.h"
#include "sqlite_callbacks.h"

void enter_interactive_mode();

int main(int argc, char *argv[])
{
	switch (argc)
	{
		case 1:
			if (file_exists(TT_DB_PATH))
			{
				enter_interactive_mode();
				break;
			}

			std::cout << "Missing: Database file for user not found in expected "
				"location: " << TT_DB_PATH << "\n" 
				"Try 'treetags plant' to create database" << std::endl;
			break;

		case 2:
			if (strcmp(argv[1], TT_CMD[HELP]) == 0)
			{
				tt_help();
				break;
			}
			else if (strcmp(argv[1], TT_CMD[EXISTS]) == 0)
			{
				tt_exists();
				break;
			}
			else if (strcmp(argv[1], TT_CMD[PLANT]) == 0)
			{
				tt_plant();
				break;
			}
			else if (strcmp(argv[1], TT_CMD[CHOP]) == 0)
			{
				tt_chop();
				break;
			}
			break;
		
		case 3:
			if (strcmp(argv[1], TT_CMD[PRINT]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_print(db, argv[2]);
					close_db(db);
				}
				break;	
			}
			break;

		case 4:
			if (strcmp(argv[1], TT_CMD[BRANCH]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_branch(db, argv[2], argv[3]);
					close_db(db);
				}
				break;
			}
			else if (strcmp(argv[1], TT_CMD[CUT]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_cut(db, argv[2], argv[3]);
					close_db(db);
				}
				break;
			}
			else if (strcmp(argv[1], TT_CMD[LEAF]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_leaf(db, argv[2], argv[3]);
					close_db(db);
				}
				break;
			}
			else if (strcmp(argv[1], TT_CMD[PLUCK]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_pluck(db, argv[2], argv[3]);
					close_db(db);
				}
				break;
			}
			break;

		case 5:
			if (strcmp(argv[1], TT_CMD[GRAFT]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_graft(db, argv[2], argv[3], argv[4]);
					close_db(db);
				}
				break;
			}
			else if (strcmp(argv[1], TT_CMD[GROW_BRANCH]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_grow_branch(db, argv[2], argv[3], argv[4]);
					close_db(db);
				}
				break;
			}
			else if (strcmp(argv[1], TT_CMD[WILT_BRANCH]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_wilt_branch(db, argv[2], argv[3], argv[4]);
					close_db(db);
				}
				break;
			}
			else if (strcmp(argv[1], TT_CMD[MEND]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_mend(db, argv[2], argv[3], argv[4]);
					close_db(db);
				}
				break;
			}
			else if (strcmp(argv[1], TT_CMD[GROW_LEAF]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_grow_leaf(db, argv[2], argv[3], argv[4]);
					close_db(db);
				}
				break;
			}									
			else if (strcmp(argv[1], TT_CMD[WILT_LEAF]) == 0)
			{
				sqlite3 *db = open_db();

				if (db != NULL)
				{
					tt_wilt_leaf(db, argv[2], argv[3], argv[4]);
					close_db(db);
				}
				break;
			}
			break;

		default:
			std::cout << "Error: Invalid command or command syntax -- " << argv[1] << "\n"
				"Try 'treetags help' for a list of commands" << std::endl;
	}

	return 0;
}

void enter_interactive_mode()
{
	bool sentinel = true;
	sqlite3 *db = open_db();

	while (sentinel)
	{
		std::vector<std::string> params;
		std::string input;

		getline(std::cin, input);
		std::stringstream ss(input);
		std::string item;
		
		while (ss >> item)
		{
			params.push_back(item);
		}

		if (params.size() != 0)
		{
			switch (params.size())
			{
				case 1:
					if (params[0].compare(QUIT) == 0)
					{
						sentinel = false;
						break;	
					}
					else if (strcmp(params[0].c_str(), TT_CMD[EXISTS]) == 0)
					{
						tt_exists();
						break;
					}
					else if (strcmp(params[0].c_str(), TT_CMD[PLANT]) == 0)
					{
						tt_plant();
						break;
					}
					else if (strcmp(params[0].c_str(), TT_CMD[CHOP]) == 0)
					{
						tt_chop();
						break;
					}
					break;
				case 2:
					if (strcmp(params[0].c_str(), TT_CMD[PRINT]) == 0)
					{
						tt_print(db, params[1]);
						break;
					}
					break;

				case 3:
					if (strcmp(params[0].c_str(), TT_CMD[BRANCH]) == 0)
					{
						tt_branch(db, const_cast<char *>(params[1].c_str()), 
							const_cast<char *>(params[2].c_str()));
						break;
					}
					else if (strcmp(params[0].c_str(), TT_CMD[CUT]) == 0)
					{
						tt_cut(db, const_cast<char *>(params[1].c_str()), 
							const_cast<char *>(params[2].c_str()));
						break;
					}
					else if (strcmp(params[0].c_str(), TT_CMD[LEAF]) == 0)
					{
						tt_leaf(db, const_cast<char *>(params[1].c_str()), 
							const_cast<char *>(params[2].c_str()));
						break;
					}
					else if (strcmp(params[0].c_str(), TT_CMD[PLUCK]) == 0)
					{
						tt_pluck(db, const_cast<char *>(params[1].c_str()), 
							const_cast<char *>(params[2].c_str()));
						break;
					}
					break;

				case 4:
					if (strcmp(params[0].c_str(), TT_CMD[GRAFT]) == 0)
					{
						tt_graft(db, const_cast<char *>(params[1].c_str()), 
							const_cast<char *>(params[2].c_str()),
							const_cast<char *>(params[3].c_str()));
						break;
					}
					else if (strcmp(params[0].c_str(), TT_CMD[GROW_BRANCH]) == 0)
					{
						tt_grow_branch(db, const_cast<char *>(params[1].c_str()), 
							const_cast<char *>(params[2].c_str()),
							const_cast<char *>(params[3].c_str()));
						break;
					}
					else if (strcmp(params[0].c_str(), TT_CMD[WILT_BRANCH]) == 0)
					{
						tt_wilt_branch(db, const_cast<char *>(params[1].c_str()), 
							const_cast<char *>(params[2].c_str()),
							const_cast<char *>(params[3].c_str()));
						break;
					}
					else if (strcmp(params[0].c_str(), TT_CMD[MEND]) == 0)
					{
						tt_mend(db, const_cast<char *>(params[1].c_str()), 
							const_cast<char *>(params[2].c_str()),
							const_cast<char *>(params[3].c_str()));
						break;
					}
					else if (strcmp(params[0].c_str(), TT_CMD[GROW_LEAF]) == 0)
					{
						tt_grow_leaf(db, const_cast<char *>(params[1].c_str()), 
							const_cast<char *>(params[2].c_str()),
							const_cast<char *>(params[3].c_str()));
						break;
					}
					else if (strcmp(params[0].c_str(), TT_CMD[WILT_LEAF]) == 0)
					{
						tt_wilt_leaf(db, const_cast<char *>(params[1].c_str()), 
							const_cast<char *>(params[2].c_str()),
							const_cast<char *>(params[3].c_str()));
						break;
					}
					break;

				default:
					std::cout << "Error: Invalid command or command syntax. Try 'help' for a "
						<< "list of commands"<< std::endl;
			}
		}
	}

	close_db(db);
}