#include "treetags_shared.h"
#include "treetags_commands.h"
#include "sqlite_callbacks.h"

const char *TT_CMD[] =
{
	"help",
	"exists",
	"plant",
	"chop",
	"branch",
	"cut",
	"graft",
	"grow-branch",
	"wilt-branch",
	"leaf",
	"pluck",
	"mend",
	"grow-leaf",
	"wilt-leaf",
	"print"
};

void tt_help()
{
	std::cout << "Status/Setup Commands: " << std::endl;
	std::cout << " -- exists\t\t\t\tCheck for existing metadata database." << std::endl;
	std::cout << " -- plant\t\t\t\tSetup metadata database." << std::endl;
	std::cout << " -- chop\t\t\t\tDestroy metadata database." << std::endl;

	std::cout << "\nBranch/Collection Commands: " << std::endl;
	std::cout << " -- branch <tags> <path>\t\tAdds a child collection with <tags> to the parent at <path>" << std::endl;
	std::cout << " -- cut <tags> <path>\t\t\tRemoves child collection with <tags> from parent at <path>" << std::endl;
	std::cout << " -- graft <tags> <src> <target>\t\tChanges collection parent from <src> to <target>" << std::endl;
	std::cout << " -- grow-branch <tags> <path> <add>\tAppend <add> to collection tags of collection"
		<< " in <path> identified by <tags>" << std::endl;
	std::cout << " -- wilt-branch <tags> <path> <remove>\tRemove <remove> from collection tags of collection"
		<< " in <path> identified by <tags>" << std::endl;

	std::cout << "\nLeaf/File Commands: " << std::endl;
	std::cout << " -- leaf <tags> <path>\t\t\tAdds a child file with <tags> to the parent at <path>" << std::endl;
	std::cout << " -- pluck <tags> <path>\t\t\tRemoves child file with <tags> from parent at <path>" << std::endl;
	std::cout << " -- mend <tags> <src> <target>\t\tChanges containing collection from <src> to <target>" << std::endl;
	std::cout << " -- grow-leaf <tags> <path> <add>\tAppend <add> to files tags of file"
		<< " in <path> identified by <tags>" << std::endl;
	std::cout << " -- wilt-leaf <tags> <path> <remove>\tRemove <remove> from file tags of file"
		<< " in <path> identified by <tags>" << std::endl;

	std::cout << " -- print <path> \t\tReturn leaves (files) immediately under <path>" << std::endl;
}

void tt_exists()
{
	if (file_exists(TT_DB_PATH))
	{
		std::cout << "Found: Database file for user found in expected "
			"location: " << TT_DB_PATH << std::endl;
	}
	else
	{
		std::cout << "Missing: Database file for user not found in expected "
			"location: " << TT_DB_PATH << "\n" 
			"Try 'treetags plant' to create database" << std::endl;
	}
}

void tt_plant()
{
	if (!file_exists(TT_DB_PATH))
	{
		if (exec_sqlite_cmd(SQL_SETUP) != 0) 
		{
			std::cout << "Error: Database schema could not be created." 
				" Check permissions for intended location: " << TT_DB_PATH 
				<< std::endl;
			return;
		}

		if (exec_sqlite_cmd(SQL_DEFAULT) == 0)
		{
			std::cout << "Success: Database file initialized at location: " 
				<< TT_DB_PATH << std::endl;
		}
		else
		{
			std::cout << "Error: Database file could not be initialized at" << std::endl;
		}
	}
	else
	{
		std::cout << "Error: Database already exists at: " << TT_DB_PATH << "\n"
			<< "Try 'treetags chop' to destroy existing database" << std::endl;
	}
}

void tt_chop()
{
	if (file_exists(TT_DB_PATH))
	{
		std::string ans;
		std::cout << "Are you certain you wish to delete the database file located at: " 
			<< TT_DB_PATH << std::endl;
		std::cout << "This will result in an unrecoverable loss of all treetags metadata "
			"for this user: yes/no >>> ";
		
		std::cin >> ans;

		if (strcmp(ans.c_str(), "yes") == 0)
		{
			if (system_returns_no_error("rm " + TT_DB_PATH + " &> /dev/null"))
			{
				std::cout << "Success: Database file was deleted." << std::endl;
			} 
			else
			{
				std::cout << "Error: Database file could not be deleted." << std::endl;
			}	
		}
		else
		{
			std::cout << "Aborted: Chop command terminated by user" << std::endl;
		}
	}
	else
	{
		std::cout << "Missing: Database file for user not found in expected "
			"location: " << TT_DB_PATH << "\n" 
			"Try 'treetags plant' to create database" << std::endl;
	}
}

void tt_branch(sqlite3 *db, char *tags, char *path)
{
	if (!regexp_attached(db))
		return;

	if (strlen(tags) == 0 || strlen(path) == 0)
	{
		std::cout << "Error: Empty arguments provided." << std::endl;
		return;
	}

	CB.initialize_collection_fields();

	int pid = 0;

	std::vector<std::string> collection_tags;
	std::vector<std::string> path_tags;
	std::unordered_set<std::string> ctag_items;
	std::unordered_set<std::string> path_items;
	std::string new_tags = "";

	// DEBUG_MSG std::cout << tags << std::endl << path << std::endl;

	if (!check_collection_tags(tags, collection_tags, ctag_items))
		return;

	// Check if path is just root
	if (strlen(path) == 1 && path[0] == '/')
	{
		pid = 0;
	}
	else 
	{
		pid = find_parent(db, path, path_tags, path_items);
		
		if (pid == -1)
			return;
	}

	// At this point, since no returns have been reached,
	// we know that an unique parent collection was found.
	// Search parent collection for existing collection with conflicting tags
	
	if (conflicting_collection_exists(db, pid, new_tags, collection_tags, ctag_items))
		return;

	if (!insert_new_collection(db, pid, new_tags))
		return;

	std::cout << "Success: Branch added." << std::endl;
}

void tt_cut(sqlite3 *db, char *tags, char *path)
{
	if (!regexp_attached(db))
		return;

	if (strlen(tags) == 0 || strlen(path) == 0)
	{
		std::cout << "Error: Empty arguments provided." << std::endl;
		return;
	}

	CB.initialize_collection_fields();

	int pid = 0;
	int cid = 0;

	std::vector<std::string> collection_tags;
	std::vector<std::string> path_tags;
	std::unordered_set<std::string> ctag_items;
	std::unordered_set<std::string> path_items;
	std::vector<std::pair<int, std::string>> collections;
	std::vector<std::pair<int, std::string>> files;
	std::string search_tags = "";
	
	if (!check_collection_tags(tags, collection_tags, ctag_items))
		return;

	// Check if path is just root
	if (strlen(path) == 1 && path[0] == '/')
	{
		pid = 0;
	}
	else 
	{
		pid = find_parent(db, path, path_tags, path_items);

		if (pid == -1)
			return;
	}

	// At this point, since no returns have been reached,
	// we know that an unique parent collection was found.
	// Search parent collection for desired collection to delete

	if (!find_collection(db, pid, search_tags, collection_tags, ctag_items))
		return;

	cid = CB.collection_id;
	search_tags = CB.ctags;

	// CHECK FOR FILES AND CHILDREN NODES
	if (find_collections_by_parent(db, cid, collections))
	{
		std::cout << "Error: Non-empty collection specified, must remove child collection(s)."
			<< std::endl;
		return;
	}

	if (find_files_by_parent(db, cid, files))
	{
		std::cout << "Error: Non-empty collection specified, must remove contained file(s)."
			<< std::endl;
		return;
	}


	// Delete child collection

	if (!delete_collection(db, cid, pid, search_tags))
	{
		std::cout << "Error: Unable to delete collection from parent" << std::endl;
		return;
	}

	if (!delete_collection_id(db, cid))
	{
		std::cout << "Error: Unable to delete collection id from collection list." 
			<< " Run 'prune' command after checking SQLite database." << std::endl;
		return;
	}

	std::cout << "Success: Branch removed." << std::endl;
}

void tt_graft(sqlite3 *db, char *tags, char *source_path, char *target_path)
{
	if (!regexp_attached(db))
		return;

	if (strlen(tags) == 0 || strlen(source_path) == 0 || strlen(target_path) == 0)
	{
		std::cout << "Error: Empty arguments provided." << std::endl;
		return;
	}

	CB.initialize_collection_fields();

	int source_id = 0;
	int target_id = 0;
	int cid = 0;

	std::vector<std::string> collection_tags;
	std::vector<std::string> source_path_tags;
	std::vector<std::string> target_path_tags;
	std::unordered_set<std::string> ctag_items;
	std::unordered_set<std::string> source_items;
	std::unordered_set<std::string> target_items;
	std::string search_tags = "";

	if (!check_collection_tags(tags, collection_tags, ctag_items))
		return;

	// Check if source path is root
	if (strlen(source_path) == 1 && source_path[0] == '/')
	{
		source_id = 0;
	}
	else
	{
		// retrieve parent id if not root
		source_id = find_parent(db, source_path, source_path_tags, source_items);

		if (source_id == -1)
			return;
	}

	CB.initialize_collection_fields();

	// Check if target path is root
	if (strlen(target_path) == 1 && target_path[0] == '/')
	{
		target_id = 0;
	}
	else
	{
		// retrieve parent id if not root
		target_id = find_parent(db, target_path, target_path_tags, target_items);

		if (target_id == -1)
			return;
	}

	// We know that both the source and target paths uniquely identify respective parent collections
	// Check for desired collection to move
	if (!find_collection(db, source_id, search_tags, collection_tags, ctag_items))
		return;	

	cid = CB.collection_id;
	search_tags = CB.ctags;



	// Ensure that no conflicts exist in new parent collection
	if (conflicting_collection_exists(db, target_id, search_tags, collection_tags, ctag_items))
		return;

	if (!reassign_collection_id(db, cid, target_id))
	{
		std::cout << "Error: Unable to move collection to new parent." <<  std::endl;
		return;
	}

	std::cout << "Success: Branch moved." << std::endl;
}

void tt_grow_branch(sqlite3 *db, char *tags, char *path, char *add_tags)
{
	if (!regexp_attached(db))
		return;

	if (strlen(tags) == 0 || strlen(path) == 0 || strlen(add_tags) == 0)
	{
		std::cout << "Error: Empty arguments provided." << std::endl;
		return;
	}

	CB.initialize_collection_fields();

	int pid = 0;
	int cid = 0;

	std::vector<std::string> collection_tags;
	std::vector<std::string> path_tags;
	std::vector<std::string> add_ctags;
	std::vector<std::string> new_ctags;
	std::unordered_set<std::string> ctag_items;
	std::unordered_set<std::string> path_items;
	std::unordered_set<std::string> atag_items;
	std::string search_tags = "";

	// Check for invalid tag syntax

	if (!check_collection_tags(tags, collection_tags, ctag_items))
		return;	

	if (!check_collection_tags(add_tags, add_ctags, atag_items))
		return;

	// Find id of parent collection

	if (strlen(path) == 1 && path[0] == '/')
	{
		pid = 0;
	}
	else
	{
		pid = find_parent(db, path, path_tags, path_items);

		if (pid == -1)
			return;
	}

	// Find collection to modify

	if (!find_collection(db, pid, search_tags, collection_tags, ctag_items))
		return;

	cid = CB.collection_id;
	search_tags = CB.ctags;

	// Determine what modified tags will look like

	if (!add_tags_to_set(search_tags, add_ctags, new_ctags))
	{
		std::cout << "Notice: Collection tags unchanged." << std::endl;
		return;
	}

	std::string empty_string = "";

	if (!update_collection_tags(db, cid, new_ctags, empty_string))
	{
		std::cout << "Error: Unable to add tags to collection tag set." << std::endl;
		return;
	}

	std::cout << "Success: Collection tag set updated." << std::endl;
}

void tt_wilt_branch(sqlite3 *db, char *tags, char *path, char *remove_tags)
{
	if (!regexp_attached(db))
		return;

	if (strlen(tags) == 0 || strlen(path) == 0 || strlen(remove_tags) == 0)
	{
		std::cout << "Error: Empty arguments provided." << std::endl;
		return;
	}

	CB.initialize_collection_fields();

	int pid = 0;
	int cid = 0;

	std::vector<std::string> collection_tags;
	std::vector<std::string> path_tags;
	std::vector<std::string> remove_ctags;
	std::vector<std::string> new_ctags;
	std::unordered_set<std::string> ctag_items;
	std::unordered_set<std::string> path_items;
	std::unordered_set<std::string> rtag_items;
	std::string search_tags = "";
	std::string modified_tags = "";

	// Check for invalid tag syntax

	if (!check_collection_tags(tags, collection_tags, ctag_items))
		return;

	if (!check_collection_tags(remove_tags, remove_ctags, rtag_items))
		return;

	// Find id of parent collection

	if (strlen(path) == 1 && path[0] == '/')
	{
		pid = 0;
	}
	else
	{
		pid = find_parent(db, path, path_tags, path_items);

		if (pid == -1)
			return;
	}

	// Find collection to modify

	if (!find_collection(db, pid, search_tags, collection_tags, ctag_items))
	{
		return;
	}

	cid = CB.collection_id;
	search_tags = CB.ctags;

	// Determine what modified tags will look like

	if (!remove_tags_from_set(search_tags, modified_tags, remove_ctags, new_ctags))
	{
		std::cout << "Notice: Collection tags unchanged." << std::endl;
		return;
	}

	// CHECK FOR CONFLICT

	if (conflicting_modified_tags(db, cid, pid, modified_tags))
		return;

	if (!update_collection_tags(db, cid, new_ctags, modified_tags))
	{
		std::cout << "Error: Unable to remove tags to collection tag set." << std::endl;
		return;
	}

	std::cout << "Success: Collection tag set updated." << std::endl;
}

void tt_leaf(sqlite3 *db, char *tags, char *path)
{
	if (!regexp_attached(db))
		return;

	if (strlen(tags) == 0 || strlen(path) == 0)
	{
		std::cout << "Error: Empty arguments provided." << std::endl;
		return;
	}

	CB.initialize_file_fields();

	int pid = 0;

	std::vector<std::string> file_tags;
	std::vector<std::string> path_tags;
	std::unordered_set<std::string> file_items;;
	std::unordered_set<std::string> path_items;
	std::string str_tags = "";

	// Check and organize given file tags
	if (!check_file_tags(tags, file_tags, file_items))
		return;

	// Check and find parent id)

	if (strlen(path) == 1 && path[0] == '/')
	{
		pid = 0;
	}
	else 
	{
		pid = find_parent(db, path, path_tags, path_items);
		
		if (pid == -1)
			return;
	}

	// check that matching file does not exist

	if (conflicting_file_exists(db, pid, str_tags, file_tags))
		return;

	// create file

	if (!insert_new_file(db, pid, str_tags))
		return;

	std::cout << "Success: File created." << std::endl;
	
}
void tt_pluck(sqlite3 *db, char *tags, char *path)
{
	if (!regexp_attached(db))
		return;

	if (strlen(tags) == 0 || strlen(path) == 0)
	{
		std::cout << "Error: Empty arguments provided." << std::endl;
		return;
	}

	CB.initialize_collection_fields();

	int cid = 0;
	int fid = 0;

	std::vector<std::string> file_tags;
	std::vector<std::string> path_tags;
	std::unordered_set<std::string> file_items;
	std::unordered_set<std::string> path_items;
	std::string str_tags = "";

	// Check and organize given file tags
	if (!check_file_tags(tags, file_tags, file_items))
		return;

	// Check and find parent id)

	if (strlen(path) == 1 && path[0] == '/')
	{
		cid = 0;
	}
	else 
	{
		cid = find_parent(db, path, path_tags, path_items);
		
		if (cid == -1)
			return;
	}

	// find unique file
	if (!find_file(db, cid, str_tags, file_tags))
		return;

	fid = CB.file_id;
	str_tags = CB.ftags;

	// Delete file

	if (!delete_file(db, fid, cid, str_tags))
	{
		std::cout << "Error: Unable to delete file from collection." << std::endl;
		return;
	}

	if (!delete_file_id(db, fid))
	{
		std::cout << "Error: Unable to delete file id from file list." 
		<< " Run 'prune' command after checking SQLite database." << std::endl;
	}

	std::cout << "Success: File removed." << std::endl;
}

void tt_mend(sqlite3 *db, char *tags, char *source_path, char *target_path)
{
	if (!regexp_attached(db))
		return;

	if (strlen(tags) == 0 || strlen(source_path) == 0 || strlen(target_path) == 0)
	{
		std::cout << "Error: Empty arguments provided." << std::endl;
		return;
	}

	CB.initialize_collection_fields();

	int source_id = 0;
	int target_id = 0;
	int fid = 0;

	std::vector<std::string> file_tags;
	std::vector<std::string> source_path_tags;
	std::vector<std::string> target_path_tags;
	std::unordered_set<std::string> file_items;;
	std::unordered_set<std::string> source_items;
	std::unordered_set<std::string> target_items;
	std::string str_tags = "";

	// Check and organize given file tags
	if (!check_file_tags(tags, file_tags, file_items))
		return;

	if (strlen(source_path) == 1 && source_path[0] == '/')
	{
		source_id = 0;
	}
	else
	{
		// retrieve parent id if not root
		source_id = find_parent(db, source_path, source_path_tags, source_items);

		if (source_id == -1)
			return;
	}

	CB.initialize_collection_fields();

	// Check if target path is root
	if (strlen(target_path) == 1 && target_path[0] == '/')
	{
		target_id = 0;
	}
	else
	{
		// retrieve parent id if not root
		target_id = find_parent(db, target_path, target_path_tags, target_items);

		if (target_id == -1)
			return;
	}

	// find unique file in using source_id
	if (!find_file(db, source_id, str_tags, file_tags))
		return;

	fid = CB.file_id;
	str_tags = CB.ftags;

	if (conflicting_file_exists(db, target_id, str_tags, target_path_tags))
		return;

	if (!reassign_file_id(db, fid, target_id))
	{
		std::cout << "Error: Unable to move file to new collection." << std::endl;
		return;
	}

	std::cout << "Success: Leaf move." << std::endl;
}

void tt_grow_leaf(sqlite3 *db, char *tags, char *path, char *add_tags)
{
	if (!regexp_attached(db))
		return;

	if (strlen(tags) == 0 || strlen(path) == 0 || strlen(add_tags) == 0)
	{
		std::cout << "Error: Empty arguments provided." << std::endl;
		return;
	}

	CB.initialize_collection_fields();

	int cid = 0;
	int fid = 0;

	std::vector<std::string> file_tags;
	std::vector<std::string> path_tags;
	std::vector<std::string> add_ftags;
	std::vector<std::string> new_ftags;
	std::unordered_set<std::string> file_items;
	std::unordered_set<std::string> path_items;
	std::unordered_set<std::string> add_items;
	std::string str_tags = "";

	// Check and organize given file tags
	if (!check_file_tags(tags, file_tags, file_items))
		return;

	if (!check_file_tags(add_tags, add_ftags, add_items))
		return;

	if (strlen(path) == 1 && path[0] == '/')
	{
		cid = 0;
	}
	else
	{
		// retrieve parent id if not root
		cid = find_parent(db, path, path_tags, path_items);

		if (cid == -1)
			return;
	}

	CB.initialize_file_fields();

	// Find file to modify
	if (!find_file(db, cid, str_tags, file_tags))
		return;

	fid = CB.file_id;
	str_tags = CB.ftags;

	// Determine what modified tags will look like

	if (!add_tags_to_set(str_tags, add_ftags, new_ftags))
	{
		std::cout << "Notice: File tags unchanged." << std::endl;
		return;
	}

	std::string empty_string = "";

	if (!update_file_tags(db, fid, new_ftags, empty_string))
	{
		std::cout << "Error: Unable to add tags to file tag set." << std::endl;
		return;
	}

	std::cout << "Success: File tag set updated." << std::endl;
}

void tt_wilt_leaf(sqlite3 *db, char *tags, char *path, char *remove_tags)
{
	if (!regexp_attached(db))
		return;

	if (strlen(tags) == 0 || strlen(path) == 0 || strlen(remove_tags) == 0)
	{
		std::cout << "Error: Empty arguments provided." << std::endl;
		return;
	}

	CB.initialize_collection_fields();

	int cid = 0;
	int fid = 0;

	std::vector<std::string> file_tags;
	std::vector<std::string> path_tags;
	std::vector<std::string> remove_ftags;
	std::vector<std::string> new_ftags;
	std::unordered_set<std::string> file_items;
	std::unordered_set<std::string> path_items;
	std::unordered_set<std::string> remove_items;
	std::string str_tags = "";
	std::string modified_tags = "";

	// Check and organize given file tags

	if (!check_file_tags(tags, file_tags, file_items))
		return;

	if (!check_file_tags(remove_tags, remove_ftags, remove_items))
		return;

	// Find id of parent collection

	if (strlen(path) == 1 && path[0] == '/')
	{
		cid = 0;
	}
	else
	{
		// retrieve parent id if not root
		cid = find_parent(db, path, path_tags, path_items);

		if (cid == -1)
			return;
	}

	CB.initialize_file_fields();

	// Find file to modify
	if (!find_file(db, cid, str_tags, file_tags))
		return;

	fid = CB.file_id;
	str_tags = CB.ftags;

	// Determine what modified tags will look like

	if (!remove_tags_from_set(str_tags, modified_tags, remove_ftags, new_ftags))
	{
		std::cout << "Notice: File tags unchanged." << std::endl;
		return;
	}

	if (conflicting_modified_file_tags(db, fid, cid, modified_tags))
		return;

	if (!update_file_tags(db, fid, new_ftags, modified_tags))
	{
		std::cout << "Error: Unable to remove tags from file tag set." << std::endl;
		return;
	}

	std::cout << "Success: File tag set updated." << std::endl;
}

void tt_print(sqlite3 *db, std::string str_query)
{
	if (!regexp_attached(db))
		return;

	if (str_query.length() == 0)
	{
		std::cout << "Error: Empty query string." << std::endl;
		return;
	}

	int cid = 0;
	std::vector<std::string> path_tags;
	std::unordered_set<std::string> path_items;
	std::vector<std::pair<int, std::string>> files;

	CB.initialize_collection_fields();

	// recursively dump all files
	if (str_query.back() == '/')
	{
		if (str_query.length() == 1 && str_query[0] == '/')
		{
			cid = 0;
		}
		else
		{
			str_query.pop_back();

			// retrieve parent id if not root
			cid = find_parent(db, const_cast<char *>(str_query.c_str()), path_tags, path_items);

			if (cid == -1)
				return;
		}

		if (find_files_by_parent(db, cid, files))
		{
			for (unsigned i = 0; i < files.size(); ++i)
			{
				std::cout << i + 1 << ": " << files.at(i).second << std::endl;
			}

			unsigned value;

			while (true)
			{
				std::cout << " > ";
				if (std::cin >> value)
				{
					if (value <= files.size() && value >= 0) {
						break;	
					}
					
				}
				std::cout << std::endl;
			}

			if (value != 0 && find_file_link(db, files.at(value - 1).first))
			{
				system_returns_no_error("nano " + CB.file_link);
			}
		}
		else 
		{
			std::cout << "Error: No files in given branch." << std::endl;
			return;
		}

	}

	//else
}