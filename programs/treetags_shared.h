// treetags_shared.h
#ifndef TREETAGS_SHARED_H
#define TREETAGS_SHARED_H

#include <regex.h>
#include <sqlite3.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>
#include <unordered_set>
#include <utility>

const std::string TT_DB_PATH = "treetags.db";
const std::string TT_ABS_PATH = "~/Dev/treetags/";
const std::string TT_ABS_PATH_OPEN = "/Dev/treetags/";
const std::string FILE_NAME = "cat /dev/urandom | tr -cd 'a-f0-9' | head -c 32 ";
const std::string WRITE_FILE = "fname.txt";
const std::string QUIT = "QUIT";

const std::string SQL_DROP = "sqlite3 " + TT_DB_PATH + " < " + 
	TT_ABS_PATH + "scripts/drop_tables.sql";
const std::string SQL_SETUP = "sqlite3 " + TT_DB_PATH + " < " + 
	TT_ABS_PATH + "scripts/create_tables.sql";
const std::string SQL_DEFAULT = "sqlite3 " + TT_DB_PATH + " < " + 
	TT_ABS_PATH + "scripts/default_values.sql";
const char VALID_PATH_CHARS[] = {'-', '0', '1', '2', '3', '4', '5',
	'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
	'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
	'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

const std::string SELECT_COLLECTION = "SELECT collection_id, collection_tags, parent_id FROM treetags_collections ";
const std::string SELECT_COLLECTION_ID = "SELECT collection_id FROM treetags_collection_ids ";
const std::string SELECT_FILE = "SELECT file_id, file_tags, collection_id FROM treetags_files ";
const std::string SELECT_FILE_ID = "SELECT file_id FROM treetags_physical_links ";
const std::string SELECT_FILE_LINK = "SELECT physical_path FROM treetags_physical_links ";
const std::string SELECT_LAST_INSERT = "SELECT last_insert_rowid() FROM treetags_collection_ids LIMIT 1;";
const std::string SELECT_LAST_FILE_INSERT = "SELECT last_insert_rowid() FROM treetags_physical_links LIMIT 1;";

const std::string WHERE_PARENT_ID = "WHERE parent_id = ";
const std::string AND_PARENT_ID = " AND parent_id = ";
const std::string WHERE_COLLECTION_ID = "WHERE collection_id = ";
const std::string AND_COLLECTION_ID = " AND collection_id = ";
const std::string AND_COLLECTION_ID_NOT = " AND collection_id != ";
const std::string WHERE_FILE_ID = "WHERE file_id = ";
const std::string AND_FILE_ID_NOT = " AND file_id != ";
const std::string AND_CTAGS_REGEXP = " AND collection_tags REGEXP ";
const std::string AND_FTAGS_REGEXP = " AND file_tags REGEXP ";

const std::string CREATE_COLLECTION_ID = "INSERT INTO treetags_collection_ids VALUES (null); ";
const std::string CREATE_FILE_ID = "INSERT INTO treetags_physical_links VALUES (null, ";
const std::string INSERT_COLLECTION = "INSERT INTO treetags_collections (collection_id "
	", parent_id, collection_tags) VALUES (";
const std::string INSERT_FILE = "INSERT INTO treetags_files (file_id, collection_id, file_tags) VALUES (";
const std::string END_VALUES = "); ";

const std::string DELETE_COLLECTION = "DELETE FROM treetags_collections ";
const std::string DELETE_COLLECTION_ID = "DELETE FROM treetags_collection_ids ";
const std::string DELETE_FILE = "DELETE FROM treetags_files ";
const std::string DELETE_FILE_ID = "DELETE FROM treetags_physical_links ";

const std::string UPDATE_COLLECTION_ID = "UPDATE treetags_collections SET parent_id = ";
const std::string UPDATE_COLLECTION_TAGS = "UPDATE treetags_collections SET collection_tags = ";
const std::string UPDATE_FILE_ID = "UPDATE treetags_files SET collection_id = ";
const std::string UPDATE_FILE_TAGS = "UPDATE treetags_files SET file_tags = ";

const std::string ORDER_COLLECTION_QUERY = " ORDER BY parent_id, collection_tags;";

bool file_exists(const std::string &name);
bool system_returns_no_error(const std::string &syscall);
void print_error(char *msg, bool is_error);
bool check_collection_tags(char *tags, std::vector<std::string> &collection_tags, std::unordered_set<std::string> &ctag_items);
bool check_file_tags(char *tags, std::vector<std::string> &file_tags, std::unordered_set<std::string> &file_items);
bool valid_tags(std::string tag);
bool add_tags_to_set(std::string &collection_tags, std::vector<std::string> &add_tags, std::vector<std::string> &new_tags);
bool remove_tags_from_set(std::string &collection_tags, std::string &str_new, std::vector<std::string> &remove_tags, std::vector<std::string> &new_tags);
std::string create_unique_file();
// functions that involve setup or handling of SQLite

sqlite3 * open_db();
bool close_db(sqlite3 *db);
void sqlite_regexp(sqlite3_context *content, int argc, sqlite3_value **values);
bool regexp_attached(sqlite3 *db);
int exec_sqlite_cmd(const std::string &cmd);
bool sqlite_returns_error(int rc, char *err);

// functions that leverage/use SQLite to run queries

int find_parent(sqlite3 *db, char *path, std::vector<std::string> &path_tags, std::unordered_set<std::string> &path_items);
bool find_collection(sqlite3 *db, int pid, std::string &select_tags, std::vector<std::string> &collection_tags, std::unordered_set<std::string> &ctag_items);
bool find_collections_by_parent(sqlite3 *db, int pid, std::vector<std::pair<int, std::string>> &collections);
bool find_file(sqlite3 *db, int cid, std::string &str_tags, std::vector<std::string> &file_tags);
bool find_files_by_parent(sqlite3 *db, int pid, std::vector<std::pair<int, std::string>> &files);
bool find_file_link(sqlite3 *db, int fid);
bool conflicting_collection_exists(sqlite3 *db, int pid, std::string &new_tags, std::vector<std::string> &collection_tags, std::unordered_set<std::string> &ctag_items);
bool conflicting_modified_tags(sqlite3 *db, int cid, int pid, std::string &tags);
bool conflicting_file_exists(sqlite3 *db, int pid, std::string &str_tags, std::vector<std::string> &file_tags);
bool conflicting_modified_file_tags(sqlite3 *db, int fid, int cid, std::string &tags);

bool insert_new_collection(sqlite3 *db, int pid, std::string &new_tags);
bool insert_new_file(sqlite3 *db, int pid, std::string &tags);

bool delete_collection(sqlite3 *db, int cid, int pid, std::string &tags);
bool delete_collection_id(sqlite3 *db, int cid);
bool delete_file(sqlite3 *db, int fid, int cid, std::string &tags);
bool delete_file_id(sqlite3 *db, int fid);

bool reassign_collection_id(sqlite3 *db, int cid, int pid);
bool reassign_file_id(sqlite3 *db, int fid, int cid);
bool update_collection_tags(sqlite3 *db, int cid, std::vector<std::string> &new_tags, std::string &str_tags);
bool update_file_tags(sqlite3 *db, int fid, std::vector<std::string> &new_tags, std::string &str_tags);

#endif
