#include "treetags_shared.h"
#include "sqlite_callbacks.h"

// file_exists code taken from http://stackoverflow.com/a/12774387

bool file_exists(const std::string &name)
{
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

bool system_returns_no_error(const std::string &syscall)
{
    int ret = system(syscall.c_str());

    if (WIFEXITED(ret) && WEXITSTATUS(ret) == 0)
      return true;

  	return false;
}

void print_error(char *msg, bool is_error) {
	if (is_error)
	{
		std::cout << "Error: ";
	}
	else
	{
		std::cout << "Success: ";
	}

	std::cout << msg << std::endl;
}

bool unique_parent_collection(sqlite3 *db, char *path, int parent_id)
{
	return true;
}

// check_collection_tags
// Iterate over each item in collection tags and check for invalid chars

bool check_collection_tags(char *tags, std::vector<std::string> &collection_tags,
	std::unordered_set<std::string> &ctag_items)
{
	char tag_delim = ',';
	std::string str_tags(tags);
	std::string ct;

	if (str_tags.find(",,", 0) != std::string::npos)
	{
		std::cout << "Error: Illegal tag syntax." << std::endl;
		return false;
	}

	if (str_tags.at(0) == tag_delim)
	{
		str_tags.erase(0, 1);
	}

	std::istringstream split(str_tags);

	while (std::getline(split, ct, tag_delim))
	{
		if (!valid_tags(ct))
		{
			std::cout << "Error: Invalid character in collection tag(s): " << ct << std::endl;
			return false;
		}

		if (!(ctag_items.find(ct) == ctag_items.end()))
		{
			std::cout << "Error: Duplicate collection tag(s): " << ct << std::endl;
			return false;;
		}

		ctag_items.insert(ct);
		collection_tags.push_back(ct);
	}

	if (collection_tags.size() < 1)
	{
		std::cout << "Error: Empty collection tags." << std::endl;
		return false;
	}

	return true;
}

bool check_file_tags(char *tags, std::vector<std::string> &file_tags,
	std::unordered_set<std::string> &file_items)
{
	char tag_delim = ',';
	std::string str_tags(tags);
	std::string ft;

	if (str_tags.find(",,", 0) != std::string::npos)
	{
		std::cout << "Error: Illegal tag syntax." << std::endl;
		return false;
	}

	if (str_tags.at(0) == tag_delim)
	{
		str_tags.erase(0, 1);
	}

	std::istringstream split(str_tags);

	while (std::getline(split, ft, tag_delim))
	{
		if (!valid_tags(ft))
		{
			std::cout << "Error: Invalid character in file tag(s): " << ft << std::endl;
			return false;
		}

		if (!(file_items.find(ft) == file_items.end()))
		{
			std::cout << "Error: Duplicate file tag(s): " << ft << std::endl;
			return false;;
		}

		file_items.insert(ft);
		file_tags.push_back(ft);
	}

	if (file_tags.size() < 1)
	{
		std::cout << "Error: Empty file tags." << std::endl;
		return false;
	}

	return true;
}

bool valid_tags(std::string tag)
{
	for (unsigned cit = 0; cit < tag.length(); ++cit)
	{
		if (strchr(VALID_PATH_CHARS, tag.at(cit)) == NULL)
			return false;
	}

	return true;
}

bool add_tags_to_set(std::string &collection_tags, std::vector<std::string> &add_tags,
	std::vector<std::string> &new_tags)
{
	char tag_delim = ',';
	std::string ct;
	std::vector<std::string> original_tags;

	if (collection_tags.at(0) == tag_delim)
	{
		collection_tags.erase(0, 1);
	}

	std::istringstream split(collection_tags);

	while (std::getline(split, ct, tag_delim))
	{
		original_tags.push_back(ct);
	}

	if (original_tags.size() < 1)
	{
		std::cout << "Error: Empty  tags." << std::endl;
		return false;
	}

	std::set_union(original_tags.begin(), original_tags.end(),
		add_tags.begin(), add_tags.end(), std::back_inserter(new_tags));

	/*std::cout << "og: ";

	for (std::string ct : original_tags) {
		std::cout << "," +  ct;
	}*/

	//std::cout << ",\nnew: ";

	for (std::string ct : new_tags)
	{
		std::cout << "," + ct;
	}

	std::cout << "," << std::endl;

	if (original_tags.size() == new_tags.size())
		return false;

	return true;
}

bool remove_tags_from_set(std::string &collection_tags, std::string &str_new, std::vector<std::string> &remove_tags,
	std::vector<std::string> &new_tags)
{
	char tag_delim = ',';
	std::string ct;
	std::vector<std::string> original_tags;

	if (collection_tags.at(0) == tag_delim)
	{
		collection_tags.erase(0, 1);
	}

	std::istringstream split(collection_tags);

	while (std::getline(split, ct, tag_delim))
	{
		original_tags.push_back(ct);
	}

	if (original_tags.size() < 1)
	{
		std::cout << "Error: Empty tags." << std::endl;
		return false;
	}

	std::set_difference(original_tags.begin(), original_tags.end(),
		remove_tags.begin(), remove_tags.end(), std::inserter(new_tags, new_tags.end()));

	/*std::cout << "og: ";

	for (std::string ct : original_tags) {
		std::cout << "," +  ct;
	}

	std::cout << ",\nnew: ";*/

	for (std::string ct : new_tags) 
	{
		str_new += "," + ct;
		//std::cout << "," + ct;
	}

	//std::cout << "," << std::endl;
	str_new += ",";
		
	if (original_tags.size() == new_tags.size())
		return false;

	if (new_tags.size() == 0)
	{
		std::cout << "Error: New tag set would be empty." << std::endl;
		return false;
	}

	return true;
}

std::string create_unique_file()
{
	std::string file_name = "";
	std::string home = getenv("HOME");
	const std::string command = FILE_NAME + "> " + home + TT_ABS_PATH_OPEN + WRITE_FILE;
	
	// DEBUG_MSG std::cout << command << std::endl;

	// create filename

	while (file_name.empty())
	{
		if (system_returns_no_error(command)) 
		{
			std::ifstream infile;
			infile.open(home + TT_ABS_PATH_OPEN + WRITE_FILE);

			if (!infile.good())
			{
				std::cout << "Error: Could not retrieve generated file name." << std::endl;
				infile.close();
				return file_name;
			}			

			std::string temp;
			getline(infile, temp);

			if (temp.empty())
			{
				std::cout << "Error: Could not retrieve generated file name." << std::endl;
				infile.close();
				return file_name;
			}

			if (!file_exists(TT_ABS_PATH + "files/" + temp + ".txt"))
			{
				file_name = home + TT_ABS_PATH_OPEN + "files/" + temp + ".txt";
			}

			infile.close();
		}
		else
		{
			std::cout << "Error: Could not retrieve generated file name." << std::endl;
			return file_name;
		}
	}

	// touch file

	if (!system_returns_no_error("touch " + file_name))
	{
		std::cout << "Error: Unable to create underlying file." << std::endl;
		return "";
	}

	return file_name;
}

sqlite3 * open_db()
{
	sqlite3 *db = NULL;
	
	if (sqlite3_open(TT_DB_PATH.c_str(), &db) == SQLITE_OK)
	{
		// DEBUG_MSG std::cout << stderr << "opened" << std::endl;
		return db;
	}
	
	std::cout << stderr << "Could not open database" << sqlite3_errmsg(db) << std::endl;

	return NULL;
}

bool close_db(sqlite3 *db)
{
	if (sqlite3_close(db) == SQLITE_OK)
	{
		// DEBUG_MSG std::cout << stderr << "closed" << std::endl;
		return true;
	}

	std::cout << stderr << "error" << sqlite3_errmsg(db) << std::endl;
	return false;
}

// sqlite_regex taken from http://stackoverflow.com/a/15529331

void sqlite_regexp(sqlite3_context *context, int argc, sqlite3_value **values) {
    int ret;
    regex_t regex;
    char* reg = (char*)sqlite3_value_text(values[0]);
    char* text = (char*)sqlite3_value_text(values[1]);

    if ( argc != 2 || reg == 0 || text == 0)
    {
        sqlite3_result_error(context, "SQL function regexp() called with invalid arguments.\n", -1);
        return;
    }

    ret = regcomp(&regex, reg, REG_EXTENDED | REG_NOSUB | REG_ICASE);
    if ( ret != 0 )
    {
        sqlite3_result_error(context, "error compiling regular expression", -1);
        return;
    }

    ret = regexec(&regex, text , 0, NULL, 0);
    regfree(&regex);

    sqlite3_result_int(context, (ret != REG_NOMATCH));
}

bool regexp_attached(sqlite3 *db)
{
	if (sqlite3_create_function(db, "REGEXP", 2, SQLITE_ANY, 0, &sqlite_regexp, NULL, NULL))
	{
		std::cout << "Error: SQLite3 failed to attach regexp function." << std::endl;
		return false;
	}

	return true;
}

int exec_sqlite_cmd(const std::string &cmd)
{
	std::cout << std::endl;
	return system(cmd.c_str());
}

bool sqlite_returns_error(int rc, char *err)
{
	if (rc) 
	{
		std::cout << "Error: SQLite3 experienced an error running a query." << std::endl;
		std::cout << err << std::endl;
		return true;
	}

	return false;
}

int find_parent(sqlite3 *db, char *path, std::vector<std::string> &path_tags, 
	std::unordered_set<std::string> &path_items)
{
	char *err = 0;
	char path_delim = '/';
	char tag_delim = ',';
	std::string str_path(path);
	std::string path_item;
	std::string sql = "";
	int pid = 0;
	int rc;

	if (str_path.find("//", 0) != std::string::npos)
	{
		std::cout << "Error: Illegal path syntax." << std::endl;
		return -1;
	}

	if (str_path.at(0) == path_delim)
	{
		str_path.erase(0, 1);
	}

	std::istringstream split_path(str_path);

	// DEBUG_MSG std::cout << "str_path: " << str_path << std::endl;

	while (std::getline(split_path, path_item, path_delim) && pid != -1)
	{
		CB.initialize_collection_fields();
		path_items.clear();
		path_tags.clear();

		std::string tag;

		if (path_item.find(",,", 0) != std::string::npos)
		{
			std::cout << "Error: Illegal tag syntax." << std::endl;
			return -1;
		}

		if (path_item.at(0) == tag_delim)
		{
			path_item.erase(0, 1);
		}

		std::istringstream split_tags(path_item);

		// Iterate over each tag in path and check for invalid chars

		// DEBUG_MSG std::cout << "path_item: " << path_item << std::endl;

		while (std::getline(split_tags, tag, tag_delim))
		{
			if (!valid_tags(tag))
			{
				std::cout << "Error: Invalid character(s) in path: " << tag << std::endl;
				return -1;
			}

			if (!(path_items.find(tag) == path_items.end()))
			{
				std::cout << "Error: Duplicate tags in path: " << tag << std::endl;
				return -1;
			}

			path_items.insert(tag);
			path_tags.push_back(tag);
		}

		std::sort(path_tags.begin(), path_tags.end());
		// DEBUG_MSG printf("%s\n", pch);

		if (path_tags.size() < 1)
		{
			std::cout << "Error: Empty path." << std::endl;
			return -1;
		}

		sql = SELECT_COLLECTION + WHERE_PARENT_ID + std::to_string(pid) + AND_CTAGS_REGEXP + "'.*,";

		for (std::string pt : path_tags)
		{
			sql += pt + "(,|(,.+,))";
		}

		sql += ".*';";

		// DEBUG_MSG std::cout << sql << std::endl;

		rc = sqlite3_exec(db, sql.c_str(), CB.find_collection_callback, (void*)&path_item, &err);

		if (CB.collection_id == -1)
		{
			std::cout << "Error: Collection at path " << path_item << " not found." << std::endl;
			return -1;
		}
		if (!CB.unique_collection)
		{
			std::cout << "Error: Path '" << path_item << "' is not distinct." << std::endl;
			return -1;
		}
		if (sqlite_returns_error(rc, err))
			return -1;

		pid = CB.collection_id;
	}

	return pid;
}

bool find_collection(sqlite3 *db, int pid, std::string &select_tags, std::vector<std::string> &collection_tags, 
	std::unordered_set<std::string> &ctag_items)
{
	char *err = 0;
	std::string sql = "";
	int rc;

	CB.initialize_collection_fields();
	std::sort(collection_tags.begin(), collection_tags.end());
	sql = SELECT_COLLECTION + WHERE_PARENT_ID + std::to_string(pid) + AND_CTAGS_REGEXP + "'.*,";

	for (std::string ct : collection_tags)
	{
		select_tags += "," + ct;
		sql += ct + "(,|(,.+,))";
	}

	select_tags += ",";
	sql += ".*'";



	rc = sqlite3_exec(db, sql.c_str(), CB.find_collection_callback, (void *)select_tags.c_str(), &err);

	if (sqlite_returns_error(rc, err)) 
		return false;


	if (CB.unique_collection && !CB.ctags.empty())
		return true;

	if (CB.unique_collection && CB.ctags.empty())
	{
		std::cout << "Error: No collection found." << std::endl;
		return false;
	}

	std::cout << "Error: Tags not uniquely identifying." << std::endl;
	return false;
}


bool find_file(sqlite3 *db, int cid, std::string &str_tags, std::vector<std::string> &file_tags)
{
	char *err = 0;
	int rc;

	CB.initialize_file_fields();
	std::sort(file_tags.begin(), file_tags.end());
	std::string sql = SELECT_FILE + WHERE_COLLECTION_ID + std::to_string(cid) + AND_FTAGS_REGEXP + "'.*,";

	for (std::string ft : file_tags)
	{
		str_tags += "," + ft;
		sql += ft + "(,|(,.+,))";
	}

	str_tags += ",";
	sql += ".*'";

	rc = sqlite3_exec(db, sql.c_str(), CB.find_file_callback, (void *)str_tags.c_str(), &err);

	if (sqlite_returns_error(rc, err)) 
		return false;

	if (CB.unique_file && !CB.ftags.empty())
		return true;

	if (CB.unique_file && CB.ftags.empty())
	{
		std::cout << "Error: No file found." << std::endl;
		return false;
	}

	std::cout << "Error: Tags not uniquely identifying." << std::endl;
	return false;
}

bool find_collections_by_parent(sqlite3 *db, int pid, std::vector<std::pair<int, std::string>> &collections)
{
	char *err = 0;
	sqlite3_stmt *stmt;
	std::string sql = SELECT_COLLECTION + WHERE_PARENT_ID + std::to_string(pid) + ";";
	int rc = sqlite3_prepare(db, sql.c_str(), -1, &stmt, NULL); 
	
    if (sqlite_returns_error(rc, err))
    {
    	sqlite3_finalize(stmt);
    	return false;
    }

    while (sqlite3_step(stmt) == SQLITE_ROW)
    {
    	const char *column_text = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 1));
    	std::string str(column_text, strlen(column_text));
    	collections.push_back(std::make_pair(sqlite3_column_int(stmt, 0), str));
	}

	if (collections.size() == 0)
	{
		sqlite3_finalize(stmt);
		return false;
	}
	
	sqlite3_finalize(stmt);
	return true;
}

bool find_files_by_parent(sqlite3 *db, int pid, std::vector<std::pair<int, std::string>> &files)
{
	char *err = 0;
	sqlite3_stmt *stmt;
	std::string sql = SELECT_FILE + WHERE_COLLECTION_ID + std::to_string(pid) + ";";
	int rc = sqlite3_prepare(db, sql.c_str(), -1, &stmt, NULL); 

    if (sqlite_returns_error(rc, err))
    {
    	sqlite3_finalize(stmt);
    	return false;
    }

    while (sqlite3_step(stmt) == SQLITE_ROW)
    {
    	const char *column_text = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 1));
    	std::string str(column_text, strlen(column_text));
    	files.push_back(std::make_pair(sqlite3_column_int(stmt, 0), str));
	}

	if (files.size() == 0)
	{
		sqlite3_finalize(stmt);
		return false;
	}

	sqlite3_finalize(stmt);
	return true;
}

bool find_file_link(sqlite3 *db, int fid)
{
	char *err = 0;
	int rc;

	std::string sql = SELECT_FILE_LINK + WHERE_FILE_ID + std::to_string(fid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.find_file_link_callback, NULL, &err);

	if (sqlite_returns_error(rc, err)) 
		return false;

	if (CB.file_link.empty())
	{
		std::cout << "Error: Returned file path was empty: " << std::endl;
		return false;
	}

	return true;
}
/*bool find_collections_by_path(sqlite *db, std::string &path, std::vector<std::pair<int, std::string>> &collections)
{
	char *err = 0;
	int rc;
	char path_delim = '/';
	char tag_delim = ',';
	std::string path_item;
	std::string sql = "";
	sqlite3_stmt *stmt;

	if (path.find("//", 0) != std::string::npos)
	{
		std::cout << "Error: Illegal path syntax." << std::endl;
		return -1;
	}

	if (path.at(0) == path_delim)
	{
		path.erase(0, 1);
	}

	if (path.back() == '/')
	{
		path.pop_back();
	}
	std::istringstream split_path(path);

	while (std::getline(split_path, path_item, path_delim) && pid != -1)
	{
		CB.initialize_collection_fields();

	}
}*/

bool conflicting_collection_exists(sqlite3 *db, int pid, std::string &new_tags, std::vector<std::string> &collection_tags, 
	std::unordered_set<std::string> &ctag_items)
{
	char *err = 0;
	std::string sql = "";
	new_tags = "";
	int rc;

	CB.initialize_collection_fields();
	std::sort(collection_tags.begin(), collection_tags.end());
	sql = SELECT_COLLECTION + WHERE_PARENT_ID + std::to_string(pid) + AND_CTAGS_REGEXP + "'.*,";

	for (std::string ct : collection_tags)
	{
		new_tags += "," + ct;
		sql += ct + "(,|(,.+,))";
	}

	new_tags += ",";
	sql += ".*'";

	rc = sqlite3_exec(db, sql.c_str(), CB.find_collection_callback, (void *)new_tags.c_str(), &err);

	if (sqlite_returns_error(rc, err)) 
		return true;

	if (!CB.ctags.empty())
	{
		std::cout << "Error: Tags not uniquely identifying, conflict with sibling collection: "
			<< CB.ctags << std::endl;
		return true;
	}

	return false;
}

bool conflicting_modified_tags(sqlite3 *db, int cid, int pid, std::string &tags)
{
	char *err = 0;
	std::string sql = SELECT_COLLECTION + WHERE_PARENT_ID + std::to_string(pid) + AND_CTAGS_REGEXP 
		+ "'" + tags + "'" + AND_COLLECTION_ID_NOT + std::to_string(cid) + ";";
	int rc;

	CB.initialize_collection_fields();

	rc = sqlite3_exec(db, sql.c_str(), CB.find_collection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return true;

	if (!CB.ctags.empty())
	{
		std::cout << "Error: New tags conflict with existing sibling." << std::endl;
		return true;
	}

	return false;
}

bool conflicting_file_exists(sqlite3 *db, int pid, std::string &str_tags,
	std::vector<std::string> &file_tags)
{
	char *err = 0;
	std::string sql = "";
	int rc;

	if (!str_tags.empty())
	{
		sql = SELECT_FILE + WHERE_COLLECTION_ID + std::to_string(pid) + AND_FTAGS_REGEXP + "'" 
			+ str_tags + "';";
	}
	else
	{
		str_tags = "";
		std::sort(file_tags.begin(), file_tags.end());

		sql = SELECT_FILE + WHERE_COLLECTION_ID + std::to_string(pid) + AND_FTAGS_REGEXP + "'.*,";

		for (std::string ft : file_tags)
		{
			str_tags += "," + ft;
			sql += ft + "(,|(,.+,))";
		}

		str_tags += ",";
		sql += ".*'";
	}

	CB.initialize_file_fields();

	rc = sqlite3_exec(db, sql.c_str(), CB.find_file_callback, NULL, &err);

	if (sqlite_returns_error(rc, err)) 
		return true;

	if (!CB.ftags.empty())
	{
		std::cout << "Error: Tags not uniquely identifying, conflict with sibling file: "
			<< CB.ftags << std::endl;
		return true;
	}

	return false;
}

bool conflicting_modified_file_tags(sqlite3 *db, int fid, int cid, std::string &tags)
{
	char *err = 0;
	std::string sql = SELECT_FILE + WHERE_COLLECTION_ID + std::to_string(cid) + AND_FTAGS_REGEXP 
		+ "'" + tags + "'" + AND_FILE_ID_NOT + std::to_string(fid) + ";";
	int rc;

	CB.initialize_file_fields();

	rc = sqlite3_exec(db, sql.c_str(), CB.find_file_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return true;

	if (!CB.ftags.empty())
	{
		std::cout << "Error: New tags conflict with existing sibling." << std::endl;
		return true;
	}

	return false;
}

bool insert_new_collection(sqlite3 *db, int pid, std::string &new_tags)
{
	char *err = 0;
	std::string sql = "";
	int rc;

	CB.initialize_collection_fields();
	sql = CREATE_COLLECTION_ID + SELECT_LAST_INSERT;

	// DEBUG_MSG std::cout << sql << std::endl;
	
	rc = sqlite3_exec(db, sql.c_str(), CB.last_insert_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	if (CB.collection_id == 0)
	{
		std::cout << "Error: Failed to create id for new collection" << std::endl;
		return false;
	}

	// Attach new collection to parent collection

	sql = INSERT_COLLECTION + std::to_string(CB.collection_id) + ", " + std::to_string(pid)
		+ ", '" + new_tags + "');";

	// DEBUG_MSG std::cout << sql << std::endl;

	rc = sqlite3_exec(db, sql.c_str(), CB.not_selection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err)) 
		return false;

	return true;
}

bool insert_new_file(sqlite3 *db, int pid, std::string &tags)
{
	char *err = 0;
	std::string sql;
	int fid = 0;
	int rc;

	CB.initialize_file_fields();

	// Create new unique underlying file

	std::string path = create_unique_file();

	if (path.empty())
		return false;

	sql = CREATE_FILE_ID + "'" + path + "'" + END_VALUES + SELECT_LAST_FILE_INSERT;

	// DEBUG_MSG std::cout << sql << std::endl;

	rc = sqlite3_exec(db, sql.c_str(), CB.last_insert_file_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	if (CB.file_id == 0)
	{
		std::cout << "Error: Failed to create id for new file" << std::endl;
		return false;
	}

	fid = CB.file_id;

	CB.initialize_file_fields();

	sql = INSERT_FILE + std::to_string(fid) + "," + std::to_string(pid) + ", '"
		+ tags + "'" + END_VALUES;

	rc = sqlite3_exec(db, sql.c_str(), CB.not_selection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	return true;
}

bool delete_collection(sqlite3 *db, int cid, int pid, std::string &tags)
{
	char *err = 0;
	int rc;
	std::string sql = DELETE_COLLECTION + WHERE_COLLECTION_ID + std::to_string(cid) + AND_PARENT_ID
		+ std::to_string(pid) + AND_CTAGS_REGEXP + "'" + tags + "';";
	rc = sqlite3_exec(db, sql.c_str(), CB.not_selection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	CB.initialize_collection_fields();

	sql = SELECT_COLLECTION + WHERE_COLLECTION_ID + std::to_string(cid);

	rc = sqlite3_exec(db, sql.c_str(), CB.find_collection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	if (CB.unique_collection && CB.ctags.empty())
		return true;

	return false;
}

bool delete_collection_id(sqlite3 *db, int cid)
{
	char *err = 0;
	int rc;
	std::string sql = DELETE_COLLECTION_ID + WHERE_COLLECTION_ID + std::to_string(cid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.not_selection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	CB.initialize_collection_fields();

	sql = SELECT_COLLECTION_ID + WHERE_COLLECTION_ID + std::to_string(cid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.find_collection_id_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	if (CB.collection_id == -1)
		return true;

	return false;
}

bool delete_file(sqlite3 *db, int fid, int cid, std::string &tags)
{
	char *err = 0;
	int rc;
	std::string sql = DELETE_FILE + WHERE_FILE_ID + std::to_string(fid) + AND_COLLECTION_ID
		+ std::to_string(cid) + AND_FTAGS_REGEXP + "'" + tags + "';";

	rc = sqlite3_exec(db, sql.c_str(), CB.not_selection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	CB.initialize_file_fields();
	sql = SELECT_FILE + WHERE_FILE_ID + std::to_string(fid);

	rc = sqlite3_exec(db, sql.c_str(), CB.find_file_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	if (CB.unique_file && CB.ftags.empty())
		return true;

	return false;
}

bool delete_file_id(sqlite3 *db, int fid)
{
	char *err = 0;
	int rc;
	std::string sql = DELETE_FILE_ID + WHERE_FILE_ID + std::to_string(fid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.not_selection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	CB.initialize_file_fields();

	sql = SELECT_FILE_ID + WHERE_FILE_ID + std::to_string(fid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.find_file_id_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	if (CB.file_id == -1)
		return true;

	return false;	
}


bool reassign_collection_id(sqlite3 *db, int cid, int pid)
{
	char *err = 0;
	int rc;
	std::string sql = UPDATE_COLLECTION_ID + std::to_string(pid) + " " +  WHERE_COLLECTION_ID
		+ std::to_string(cid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.not_selection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	CB.initialize_collection_fields();

	sql = SELECT_COLLECTION + WHERE_COLLECTION_ID + std::to_string(cid) + 
		AND_PARENT_ID + std::to_string(pid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.find_collection_id_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	if (CB.collection_id == -1)
		return true;

	return false;
}

bool reassign_file_id(sqlite3 *db, int fid, int cid)
{
	char *err = 0;
	int rc;
	std::string sql = UPDATE_FILE_ID + std::to_string(cid) + " " + WHERE_FILE_ID 
		+ std::to_string(fid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.not_selection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	CB.initialize_file_fields();

	sql = SELECT_FILE + WHERE_FILE_ID + std::to_string(fid) + 
		AND_COLLECTION_ID + std::to_string(cid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.find_file_id_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	if (CB.file_id == -1)
		return true;

	return false;
}

bool update_collection_tags(sqlite3 *db, int cid, std::vector<std::string> &new_tags, std::string &str_tags)
{
	char *err = 0;
	int rc;
	std::string sql = UPDATE_COLLECTION_TAGS + "'";

	if (str_tags.length() != 0)
	{
		sql += str_tags;
	}
	else
	{
		for (std::string ct : new_tags)
		{
			sql += "," + ct;		
		}

		sql += ",";
	}

	sql += "'" + WHERE_COLLECTION_ID + std::to_string(cid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.not_selection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	return true;
}

bool update_file_tags(sqlite3 *db, int fid, std::vector<std::string> &new_tags, std::string &str_tags)
{
	char *err = 0;
	int rc;
	std::string sql = UPDATE_FILE_TAGS + "'";

	if (str_tags.length() != 0)
	{
		sql += str_tags;
	}
	else
	{
		for (std::string ft : new_tags)
		{
			sql += "," + ft;		
		}

		sql += ",";
	}

	sql += "'" + WHERE_FILE_ID + std::to_string(fid) + ";";

	rc = sqlite3_exec(db, sql.c_str(), CB.not_selection_callback, NULL, &err);

	if (sqlite_returns_error(rc, err))
		return false;

	return true;
}
