// treetags_commands.h
#ifndef TREETAGS_COMMANDS_H
#define TREETAGS_COMMANDS_H

enum
{	
	HELP,
	EXISTS,
	PLANT,
	CHOP,
	BRANCH,
	CUT,
	GRAFT,
	GROW_BRANCH,
	WILT_BRANCH,
	LEAF,
	PLUCK,
	MEND,
	GROW_LEAF,
	WILT_LEAF,
	PRINT,
	CMD_COUNT
};

extern const char *TT_CMD[CMD_COUNT];

void tt_help();
void tt_exists();
void tt_plant();
void tt_chop();
void tt_branch(sqlite3 *db, char *tags, char *path);
void tt_cut(sqlite3 *db, char *tags, char *path);
void tt_graft(sqlite3 *db, char *tags, char *path_src, char *path_target);
void tt_grow_branch(sqlite3 *db, char *tags, char *path, char *add_tags);
void tt_wilt_branch(sqlite3 *db, char *tags, char *path, char *remove_tags);
void tt_leaf(sqlite3 *db, char *tags, char *path);
void tt_pluck(sqlite3 *db, char *tags, char *path);
void tt_mend(sqlite3 *db, char *tags, char *path_src, char *path_target);
void tt_grow_leaf(sqlite3 *db, char *tags, char *path, char *add_tags);
void tt_wilt_leaf(sqlite3 *db, char *tags, char *path, char *add_tags);
void tt_print(sqlite3 *db, std::string str_query);

#endif
